;define([
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/quote'

], function (Component, ko, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/shipping-info'
        },

        initObservable: function () {
            var self = this._super();
            this.showShippingInfo = ko.computed(function() {

                var country = quote.shippingAddress();

                if(country && country.countryId !== undefined) {
                    if(country.countryId === 'AE') {
                        return true;
                    }
                }

                return false;

            }, this);

            return this;
        }
    });
});